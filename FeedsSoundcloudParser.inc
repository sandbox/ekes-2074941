<?php

/**
 * @file
 * Feeds parser class for Soundcloud.
 */

/**
 * Class definition for Soundcloud Parser.
 */
class FeedsSoundcloudParser extends FeedsParser {

  /**
   * Parse the extra mapping sources provided by this parser.
   *
   * @param $batch FeedsImportBatch
   * @param $source FeedsSource
   *
   * @see FeedsParser::parse()
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $result) {

    $items = $result->getRaw();

    // Was retrieved by a more normal fetcher.
    if (! is_array($items)) {
      $items = json_decode($items);
    }

    $rows = array();
    foreach ($items as $item) {
      // Sort tags
      // Operating as SoundCloud UI does the first tag is what is stored in
      // the genre.
      $split_tags = $this->parseTagString('"' . $item['genre'] . '" ' . $item['tag_list']);
      $item['tags'] = $split_tags['tags'];
      // Machine tags like this will need post processing. Leaving in the
      // array for anyone who is implementing hooks to use as they wish.
      $item['machine_tags'] = $split_tags['machine_tags'];
      // Make the most common, lon/lat available for geofield.
      $lon = $lat = NULL;
      if (isset($split_tags['machine_tags']['geo']['lon'][0])) {
        $lon = $split_tags['machine_tags']['geo']['lon'][0];
      }
      if (isset($split_tags['machine_tags']['geo']['lat'][0])) {
        $lat = $split_tags['machine_tags']['geo']['lat'][0];
      }
      if ($lon && $lat) {
        $item['geo'] = "POINT($lon $lat)";
      }
      else {
        $item['geo'] = '';
      }
      // User & Label mini user representations
      foreach ( array('user', 'label') as $type) {
        if (isset($item[$type]) && is_array($item[$type])) {
          $item[$type . '-id'] = $item[$type]['id'];
          $item[$type . '-kind'] = $item[$type]['kind'];
          $item[$type . '-permalink'] = $item[$type]['permalink'];
          $item[$type . '-username'] = $item[$type]['username'];
          $item[$type . '-uri'] = $item[$type]['uri'];
          $item[$type . '-permalink_url'] = $item[$type]['permalink_url'];
          $item[$type . '-avatar_url'] = $item[$type]['avatar_url'];
        }
      }

      $item['timestamp'] = strtotime($item['created_at']);
      if (empty($item['release_year'])) {
        $item['release_year'] = date('Y', $item['timestamp']);
      }
      if (empty($item['release_month'])) {
        $item['release_month'] = date('n', $item['timestamp']);
      }
      else {
        $item['release_month'] = str_pad($item['release_month'], 2, '0', STR_PAD_LEFT);
      }
      if (empty($item['release_day'])) {
        $item['release_day'] = date('j', $item['timestamp']);
      }
      else {
        $item['release_day'] = str_pad($item['release_day'], 2, '0', STR_PAD_LEFT);
      }
      $item['release_date'] = $item['release_year'] . '-' . $item['release_month'] . '-' . $item['release_day'];
      $rows[] = $item;
    }

    return new FeedsParserResult($rows, $source->feed_nid);
  }

  /**
   * Parses a String of Tags.
   *
   * @author unknown
   *
   * Tags are space delimited. Either single or double quotes mark a phrase.
   * Odd quotes will cause everything on their right to reflect as one single
   * tag or phrase. All white-space within a phrase is converted to single
   * space characters. Quotes burried within tags are ignored! Duplicate tags
   * are ignored, even duplicate phrases that are equivalent.
   * Returns an array of tags.
   *
   * "test one" "what happens " with quotes in "test two"
   */
  /**
   * Helper function: Split tag_list string.
   *
   * Tag lists include single tags separated by spaced, tags with more
   * than one word in double-quotes, and machine tags.
   * https://developers.soundcloud.com/docs/api/reference#tag_list
   *
   * Example. Tag string:
   *   one two "three" "four five" six-seven ns:k=val "ns:k=val space"
   *   "namespace:key=value assigned here"
   * Will return:
   * array (
   *   'tags' => array (
   *     0 => 'one', 1 => 'two', 2 => 'six-seven', 3 => 'three', 4 => 'four five',
   *   ),
   *   'machine_tags' => array (
   *     'ns' => array (
   *       'k' => array (
   *         0 => 'val', 1 => 'val space',
   *       ),
   *     ),
   *     'namespace' => array (
   *       'key' => array (
   *         0 => 'value assigned here',
   *       ),
   *     ),
   *   ),
   * )
   *
   * Note SC doesn't accept " within tags (it will split the tag at a double quote
   * so that does not need to be taken into account.
   *
   * @param string $tag_list
   *   Tags as returned by Soundcloud API.
   *
   * @return array
   *   Keyed array of tags and machine tags seperated. See example.
   */
  private function parseTagString($tag_list) {
    $filtered_tags = array('tags' => array(), 'machine_tags' => array());

    // First split by spaces not within quotes.
    $matches = array();
    preg_match_all('/([^\s"]+)|"([^"]*)"/', $tag_list, $matches);
    $tags = array_merge(array_filter($matches[1]), array_filter($matches[2]));
    foreach ($tags as $tag) {
      $matches = array();
      // (namespace):(key)=(value).
      if (preg_match('/^([^\s]+)\:([^\s]+)\=(.*)$/', $tag, $matches)) {
        $filtered_tags['machine_tags'][$matches[1]][$matches[2]][] = $matches[3];
      }
      else {
        $filtered_tags['tags'][] = $tag;
      }
    }

    return $filtered_tags;
  }

  /**
   * Add the extra mapping sources provided by this parser.
   *
   * http://developers.soundcloud.com/docs/api/reference#tracks Properties
   * name  description   example value
   * id  integer ID  123
   * created_at  timestamp of creation   "2009/08/13 18:30:10 +0000"
   * user_id   user-id of the owner  343
   * user  mini user representation of the owner
   *   {id: 343, username: "Doctor Wilson"...}
   *   [user] => Array
   *     (
   *      [id] => 345
   *      [kind] => user
   *      [permalink] => user-permalink
   *      [username] => User Name
   *      [uri] => https://api.soundcloud.com/users/345
   *      [permalink_url] => http://soundcloud.com/user-permalink
   *      [avatar_url] => https://i1.sndcdn.com/avatars-123-large.jpg
   *     )
   * title   track title   "S-Bahn Sounds"
   * permalink   permalink of the resource   "sbahn-sounds"
   * permalink_url   URL to the SoundCloud.com page
   *   "http://soundcloud.com/bryan/sbahn-sounds"
   * uri   API resource URL  "http://api.soundcloud.com/tracks/123"
   * sharing   public/private sharing  "public"
   * embeddable_by   who can embed this track or playlist
   *   "all", "me", or "none"
   * purchase_url  external purchase link  "http://amazon.com/buy/a43aj0b03"
   * artwork_url   URL to a JPEG image
   *   "http://i1.sndcdn.com/a....-large.jpg?142a848"
   * description   HTML description  "my first track"
   * label   label mini user object  {id:123, username: "BeatLabel"...}
   * duration  duration in milliseconds  1203400
   * genre   genre   "HipHop"
   * shared_to_count   number of sharings (if private)   45
   * tag_list  list of tags  "tag1 \"hip hop\" geo:lat=32.444 geo:lon=55.33"
   * label_id  id of the label user  54677
   * label_name  label name  "BeatLabel"
   * release   release number  3234
   * release_day   day of the release  21
   * release_month   month of the release  5
   * release_year  year of the release   2001
   * streamable  streamable via API (boolean)  true
   * downloadable  downloadable (boolean)  true
   * state   encoding state  "finished"
   * license   creative common license   "no-rights-reserved"
   * track_type  track type  "recording"
   * waveform_url  URL to PNG waveform image
   *   "http://w1.sndcdn.com/fxguEjG4ax6B_m.png"
   * download_url  URL to original file
   *   "http://api.soundcloud.com/tracks/3/download"
   * stream_url  link to 128kbs mp3 stream
   *   "http://api.soundcloud.com/tracks/3/stream"
   * video_url   a link to a video page
   *   "http://vimeo.com/3302330"
   * bpm   beats per minute  120
   * commentable   track commentable (boolean)   true
   * isrc  track ISRC  "I123-545454"
   * key_signature   track key   "Cmaj"
   * comment_count   track comment count   12
   * download_count  track download count  45
   * playback_count  track play count  435
   * favoritings_count   track favoriting count  6
   * original_format   file format of the original file  "aiff"
   * original_content_size   size in bytes of the original file  10211857
   * created_with  the app that the track created  {"id"=>3434, "..."=>nil}
   * asset_data  binary data of the audio file   (only for uploading)
   * artwork_data  binary data of the artwork image  (only for uploading)
   * user_favorite   track favorite of current user
   *   (boolean, authenticated requests only)   1
   **/
  public function getMappingSources() {
    $mapping = parent::getMappingSources() + array(
      'id' =>  array(
        'name' => t('ID'),
        'description' => t('Integer ID eg. 123'),
      ),
      'created_at' => array(
        'name' => t('Created at'),
        'description' => t('Timestamp of creation, Soundcloud format eg. "2009/08/13 18:30:10 +0000"'),
      ),
      'timestamp' => array(
        'name' => t('Timestamp'),
        'description' => t('Timestamp of creation. Unix timestamp format.'),
      ),
       'user_id' => array(
        'name' => t('Soundcloud user ID'),
        'description' => t('User-id of the owner eg. 343'),
      ),
      'title' => array(
        'name' => t('Title'),
        'description' => t('Track title eg. "S-Bahn Sounds"'),
      ),
      'permalink' => array(
        'name' => t('Permalink'),
        'description' => t('Permalink path of the resource eg. "sbahn-sounds"'),
      ),
      'permalink_url' => array(
        'name' => t('Permalink URL'),
        'description' => t('URL to the SoundCloud.com page eg. "http://soundcloud.com/bryan/sbahn-sounds"'),
      ),
      'uri' => array(
        'name' => t('URI'),
        'description' => t('API resource URL eg. "http://api.soundcloud.com/tracks/123"'),
      ),
      'sharing' => array(
        'name' => t('Public/private sharing'),
        'description' => t('eg. "public"'),
      ),
      'embeddable_by' => array(
        'name' => t('Embeddable by'),
        'description' => t('Who can embed this track or playlist  "all", "me", or "none"'),
      ),
      'purchase_url' => array(
        'name' => t('Purchase URL'),
        'description' => t('External purchase link eg. "http://amazon.com/buy/a43aj0b03"'),
      ),
      'artwork_url' => array(
        'name' => t('Artwork URL'),
        'description' => t('URL to a JPEG image eg. "http://i1.sndcdn.com/a....-large.jpg?142a848"'),
      ),
      'description' => array(
        'name' => t('Description'),
        'description' => t('HTML description eg. "my first track"'),
      ),
      'duration' => array(
        'name' => t('Duration'),
        'description' => t('In milliseconds eg. 1203400'),
      ),
      'genre' => array(
        'name' => t('Genre'),
        'description' => t('eg. "HipHop"'),
      ),
      'shared_to_count' => array(
        'name' => t('Number of sharings'),
        'description' => t('If private eg. 45'),
      ),
      'tag_list' => array(
        'name' => t('Tags'),
        'description' => t('List of tags  "tag1 \"hip hop\" geo:lat=32.444 geo:lon=55.33"'),
      ),
      'tags' => array(
        'name' => t('Tags: taxonomy term'),
        'description' => t('List of tags formatted for Taxonomy Term input'),
      ),
      'geo' => array(
        'name' => t('Location: WKT'),
        'description' => t('Location tags formatted as WKT for geofield.'),
      ),
      'label_id' => array(
        'name' => t('Label ID'),
        'description' => t('Soundcloud ID of the label user eg. 54677'),
      ),
      'label_name' => array(
        'name' => t('Label name'),
        'description' => t('eg. "BeatLabel"'),
      ),
      'release' => array(
        'name' => t('Release number'),
        'description' => t('eg. 3234'),
      ),
      'release_day' => array(
        'name' => t('Release day'),
        'description' => t('Day of release eg. 21'),
      ),
      'release_month' => array(
        'name' => t('Release month'),
        'description' => t('Month of the release eg. 5'),
      ),
      'release_year' => array(
        'name' => t('Release year'),
        'description' => t('Year of the release eg. 2001'),
      ),
      'release_date' => array(
        'name' => t('Release date'),
        'description' => t('Release year-month-day combined. eg 2001-05-21'),
      ),
      'streamable' => array(
        'name' => t('Streamable'),
        'description' => t('If streamable via API (boolean) eg. true'),
      ),
      'downloadable' => array(
        'name' => t('Downloadable'),
        'description' => t('If downloadable (boolean) eg. true'),
      ),
      'state' => array(
        'name' => t('Encoding state'),
        'description' => t('eg. "finished"'),
      ),
      'license' => array(
        'name' => t('Creative commons license'),
        'description' => t('eg. "no-rights-reserved"'),
      ),
      'track_type' => array(
        'name' => t('Track type'),
        'description' => t('eg. "recording"'),
      ),
      'waveform_url' => array(
        'name' => t('Waveform URL'),
        'description' => t('URL to PNG waveform image eg. "http://w1.sndcdn.com/fxguEjG4ax6B_m.png"'),
      ),
      'download_url' => array(
        'name' => t('Download URL'),
        'description' => t('URL to original file eg. "http://api.soundcloud.com/tracks/3/download"'),
      ),
      'stream_url' => array(
        'name' => t('Stream URL'),
        'description' => t('Link to 128kbs mp3 stream eg. "http://api.soundcloud.com/tracks/3/stream"'),
      ),
      'video_url' => array(
        'name' => t('Video URL'),
        'description' => t('A link to a video page eg. "http://vimeo.com/3302330"'),
      ),
      'bpm' => array(
        'name' => t('Beats per minute'),
        'description' => t('eg. 120'),
      ),
      'commentable' => array(
        'name' => t('Commentable'),
        'description' => t('Track commentable (boolean) eg. true'),
      ),
      'isrc' => array(
        'name' => t('Track ISRC'),
        'description' => t('eg. "I123-545454"'),
      ),
      'key_signature' => array(
        'name' => t('Track key'),
        'description' => t('eg. "Cmaj"'),
      ),
      'comment_count' => array(
        'name' => t('Track comment count'),
        'description' => t('eg. 12'),
      ),
      'download_count' => array(
        'name' => t('Track download count'),
        'description' => t('eg. 45'),
      ),
      'playback_count' => array(
        'name' => t('Track play count'),
        'description' => t('eg. 435'),
      ),
      'favoritings_count' => array(
        'name' => t('Track favoriting count'),
        'description' => t('eg. 6'),
      ),
      'original_format' => array(
        'name' => t('Original format'),
        'description' => t('File format of the original file eg. "aiff"'),
      ),
      'original_content_size' => array(
        'name' => t('Original content size'),
        'description' => t('Size in bytes of the original file eg. 10211857'),
      ),
      'created_with' => array(
        'name' => t('Created with'),
        'description' => t('The app that the track created eg. {"id"=>3434, "..."=>nil}'),
      ),
      'user_favorite' => array(
        'name' => t('Favorite track'),
        'description' => t('Track favorite of current user (boolean, authenticated requests only) eg. 1'),
      ),
    );

    foreach (array('user', 'label') as $type) {
      $mapping[$type . '-username'] = array(
        'name' => t('!type: Soundcloud user name', array('!type' => $type)),
      );
      $mapping[$type . '-permalink'] = array(
        'name' => t('!type: Permalink', array('!type' => $type)),
        'description' => t('Permalink path eg. user-permalink'),
      );
      $mapping[$type . '-uri'] = array(
        'name' => t('!type: User URI', array('!type' => $type)),
        'dsecription' => t('Users soundcloud URI'),
      );
      $mapping[$type . '-permalink_url'] = array(
        'name' => t('!type: User permalink URL', array('!type' => $type)),
        'description' => t('Full permalink URL eg. http://soundcloud.com/user-permalink'),
      );
      $mapping[$type . '-avatar_url'] = array(
        'name' => t('!type: User avatar URL', array('!type' => $type)),
        'description' => t('Full URL to soundcloud avatar.'),
      );
    }

    return $mapping;
  }
}
