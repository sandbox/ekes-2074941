Feeds Soundcloud
================

Soundcloud API integration for Drupal Feeds Module.

The module adds an fetcher and parser. The fetcher will retrieve items from
a Soundcloud API uri. The parser will parse Soundcloud Tracks retrieved by
the fetcher.

From the parser you can map all the track fields made available by Soundcloud
using standard Feeds mapping.

Notes
-----

* You need to know the API uri. Documentation eg.:-
    * http://developers.soundcloud.com/docs/api/reference#tracks
    * http://developers.soundcloud.com/docs/api/reference#playlists
    * http://developers.soundcloud.com/docs/api/reference#groups
* If oauth is needed the module is using just one key stored in variable
'feeds_soundcloud_site_oauth_nonexpire'. Documentation on how to generate a 
non-expiring oauth key: 
  http://developers.soundcloud.com/docs/api/reference#token

Further development
-------------------

For the moment the module works for what it was written. To make a full module
the two notes above would have to become more user friendly, and flexible.

* The input page could accept IDs and work out the API URI from it.
* The OAUTH key could be generated from <a href="http://drupal.org/project/soundcloud>Soundcloud connect</a>
module users account; or from an admin page with the site account.
