<?php

/**
 * Batch importer.
 */
class FeedsSoundcloudResult extends FeedsFetcherResult {
  protected $path;
  protected $limit;
  protected $latest;
  protected $client;
  protected $stats;

  /**
   * Constructor.
   */
  public function __construct($path = NULL, $uid = 0, $limit = 200, $latest = TRUE) {
    $this->path = $path;
    $this->limit = $limit;
    $this->latest = $latest;
    if ($uid) {
      $this->client = soundcloud_create_soundcloud($uid);
    }
    if (empty($this->client)) {
      $this->client = soundcloud_initialize();
    }
    $this->stats = new FeedsSoundcloudStatistics($path, $latest);
  }

  /**
   * It can only make sense to return an array here.
   *
   * Soundcloud has a limit of 200 items; so unless we manually join json
   * strings it's best to decode them here.
   */
  public function getRaw() {
    $this->stats->newBatch();

    $output = array();
    $limit = $this->limit > 200 ? 200 : $this->limit;
    $query['limit'] = $limit;
    // If checking full feed, start from saved offset.
    if (!$this->latest) {
      $start = $query['offset'] = variable_get('feeds_soundcloud_offset_' . sha1($this->path), 0);
    }
    else {
      $start = $query['offset'] = 0;
    }

    do {
      $result = $this->client->get($this->path, $query);
      $items = json_decode($result, TRUE);
      $output = array_merge($output, $items);
    } while (
      (($query['offset'] += $limit) - $start) < $this->limit &&
      count($items)
    );

    $this->stats->setBatchItems(count($output));
    // If checking full feed, save offset if not reached the end.
    if (!$this->latest) {
      $final_offset = count($items) ? $query['offset'] : 0;
      variable_set('feeds_soundcloud_offset_' . sha1($this->path), $final_offset);
      $this->stats->setBatchOffset($final_offset);
    }
    $this->stats->saveBatch();

    return $output;
  }
}

/**
 * Fetches data via HTTP.
 */
class FeedsSoundcloudFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    if (!empty($source->feed_nid)) {
      $feed_node = node_load($source->feed_nid);
      $uid = $feed_node->uid;
    }
    else {
      $uid = 0;
    }
    return new FeedsSoundcloudResult($source_config['source'], $uid, $source_config['limit'], (bool) $source_config['latest']);
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('Soundcloud API Path'),
      '#description' => t(
        'For example /users/{id}/tracks, /groups/{id}/tracks. See <a href="!url">API Documentation</a> for more information.',
        array('!url' => url('https://developers.soundcloud.com/docs/api/reference'))
      ),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    $form['limit'] = array(
      '#type' => 'select',
      '#title' => t('Limit'),
      '#description' => t('Maximum number of items to check per request.'),
      '#default_value' => isset($source_config['limit']) ? $source_config['limit'] : 200,
      '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 10 => 10, 20 => 20, 50 => 50, 100 => 100, 200 => 200, 600 => 600, 800 => 800, 1000 => 1000, 2000 => 2000, 3000 => 3000, 4000 => 4000, 6000 => 6000, 8000 => 8000),
    );
    $form['latest'] = array(
      '#type' => 'radios',
      '#title' => t('Check full feed'),
      '#description' => t('Soundcloud lists feeds in order of creation date. If the limit is less than the number of items in the feed changes to older items will not be downlodaded if all items are not checked.'),
      '#default_value' => isset($source_config['latest']) ? $source_config['latest'] : 1,
      '#options' => array(0 => t('All items.'), 1 => t('Latest created items.')),
    );

    if (!empty($source_config['source'])) {
      $stats_class = new FeedsSoundcloudStatistics($source_config['source'], $source_config['latest']);
      $stats = $stats_class->get();

      $form['stats'] = array(
        '#type' => 'fieldset',
        '#title' => t('Feed statistics'),
      );
      $form['stats']['class'] = array(
        '#type' => 'value',
        '#value' => $stats_class,
      );
      $form['stats']['fetch_count'] = array(
        '#type' => 'item',
        '#title' => t('Number of fetches'),
        '#markup' => $stats['fetch_count'],
      );
      $form['stats']['time'] = array(
        '#type' => 'item',
        '#title' => t('Time taken to fetch: average (last)'),
        '#markup' => $stats['time_total'] / $stats['fetch_count'] . ' (' . $stats['time_last'] . ')',
      );
      $form['stats']['items'] = array(
        '#type' => 'item',
        '#title' => t('Items fetched: average (last)'),
        '#markup' => $stats['items_total'] / $stats['fetch_count'] . ' (' . $stats['items_last'] . ')',
      );
      $form['stats']['timestamp'] = array(
        '#type' => 'item',
        '#title' => t('Last fetch time'),
        '#markup' => format_date($stats['last_timestamp']),
      );
       if (!$source_config['latest']) {
        $form['stats']['all'] = array(
          '#type' => 'fieldset',
          '#title' => t('Full feed, totals for all items'),
        );
        $form['stats']['all']['feed_all_fetch_count'] = array(
          '#type' => 'item',
          '#title' => t('Number of full feed fetches'),
          '#markup' => $stats['feed_all_fetch_count'],
        );
        $form['stats']['all']['fetch_count'] = array(
          '#type' => 'item',
          '#title' => t('Number of fetches for full feed: average (last)'),
          '#markup' => $stats['fetch_count_all_total'] / $stats['feed_all_fetch_count']
            . ' (' . $stats['fetch_count_all_last'] . ')',
        );
        $form['stats']['all']['time'] = array(
          '#type' => 'item',
          '#title' => t('Combined time taken to fetch: average (last)'),
          '#markup' => $stats['time_all_total'] / $stats['feed_all_fetch_count']
            . ' (' . $stats['time_all_last'] . ')',
        );
        $form['stats']['all']['items'] = array(
          '#type' => 'item',
          '#title' => t('Number of items in full feed: average (ast)'),
          '#markup' => $stats['items_all_total'] / $stats['feed_all_fetch_count']
            . '(' . $stats['items_all_last'] . ')',
        );
        $form['stats']['all']['timestamp'] = array(
          '#type' => 'item',
          '#title' => t('Last completed feed fetch time'),
          '#markup' => format_date($stats['last_timestamp_all']),
        );
      }
      $form['stats']['reset'] = array(
        '#type' => 'checkbox',
        '#title' => t('Reset statistics'),
      );
    }
    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    $path = $values['source'] = trim($values['source']);
    // If attached to a node, and there is a user - see
    // feeds_soundcloud_node_validate() - check access as this user,
    // if they have connected to SoundCloud.
    if (!empty($values['uid'])) {
      $client = soundcloud_create_soundcloud($values['uid']);
    }
    if (empty($client)) {
      $client = soundcloud_initialize();
    }
    try {
      $client->get($path, array('limit' => 1));
    }
    catch (SoundCloud\InvalidHttpResponseCodeException $e) {
      $form_key = 'feeds][' . get_class($this) . '][source';
      form_set_error(
        $form_key,
        t(
          'The path %path is invalid. %error.',
          array(
            '%path' => $path,
            '%error' => $e->getMessage(),
          )
        )
      );
    }

    if ($values['stats']['reset']) {
      $values['stats']['class']->reset();
      $values['stats']['reset'] = 0;
    }
  }
}

/**
 * Statistics for Fetcher.
 */
class FeedsSoundcloudStatistics {
  protected $batch_timer;
  protected $batch_items;
  protected $batch_offset;

  protected $path;
  protected $limit;

  public function __construct($path, $limit) {
    $this->path = $path;
    $this->limit = $limit;
  }

  public function newBatch() {
    $this->batch_timer = microtime(true);
  }

  public function saveBatch() {
    $stats = $this->get();
    $time = microtime(true) - $this->batch_timer;

    $stats['fetch_count']++;
    $stats['time_last'] = $time;
    $stats['time_total'] += $time;
    $stats['items_last'] = $this->batch_items;
    $stats['items_total'] += $this->batch_items;
    $stats['last_timestamp'] = time();

    if (!$this->latest) {
      $stats['fetch_count_all_current']++;
      $stats['time_all_current'] += $time;
      $stats['items_all_current'] += $this->batch_items;
      if (!$this->batch_offset) {
        $stats['feed_all_fetch_count']++;
        $stats['fetch_count_all_last'] = $stats['fetch_count_all_current'];
        $stats['fetch_count_all_total'] += $stats['fetch_count_all_current'];
        $stats['fetch_count_all_current'] = 0;
        $stats['time_all_last'] = $stats['time_all_current'];
        $stats['time_all_total'] += $stats['time_all_current'];
        $stats['time_all_current'] = 0;
        $stats['items_all_last'] = $stats['items_all_current'];
        $stats['items_all_total'] += $stats['items_all_current'];
        $stats['items_all_current'] = 0;
        $stats['last_timestamp_all'] = time();
      }
    }

    variable_set('feeds_soundcloud_stats_' . sha1($this->path), $stats);
  }

  public function setBatchItems($count) {
    $this->batch_items = $count;
  }

  public function setBatchOffset($offset) {
    $this->batch_offset = $offset;
  }

  public function get() {
    if($stats = variable_get('feeds_soundcloud_stats_' . sha1($this->path), FALSE)) {
      return $stats;
    }
    else {
      $stats = array();
      // Number of ::fetch() runs in statistics.
      $stats['fetch_count'] = 0;
      // Microtime of last run.
      $stats['time_last'] = 0;
      // Microtime of all runs.
      $stats['time_total'] = 0;
      // Items retrieved in last run.
      $stats['items_last'] = 0;
      // Total number of items all runs.
      $stats['items_total'] = 0;
      // Timestamp of last ::fetch().
      $stats['last_timestamp'] = 0;
      if (!$this->latest) {
        $stats['feed_all_fetch_count'] = 0;

        $stats['fetch_count_all_current'] = 0;
        $stats['fetch_count_all_last'] = 0;
        $stats['fetch_count_all_total'] = 0;

        $stats['time_all_current'] = 0;
        $stats['time_all_last'] = 0;
        $stats['time_all_total'] = 0;

        $stats['items_all_current'] = 0;
        $stats['items_all_last'] = 0;
        $stats['items_all_total'] = 0;

        $stats['last_timestamp_all'] = 0;
      }
    }
  }

  public function reset() {
    variable_del('feeds_soundcloud_offset_' . sha1($this->path));
    variable_del('feeds_soundcloud_stats_' . sha1($this->path));
  }
}
